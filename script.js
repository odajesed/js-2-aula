//Q3
gods.sort((a, b) => (a.pantheon > b.pantheon) ? 1 : -1)
for(god of gods) {
    console.log(god.pantheon)
    console.log(`o Deus ${god.name} é ${god.pantheon}`)
}

//Q4

let deuses = []

for( god of gods) {
    deuses.push(`${god.name} (${god.class})`)
}
console.log(deuses)

//Q5

let deusesGHM = []

for(god of gods) {
    if(god.class === "Hunter" && god.pantheon === "Greek" && god.roles ==="Mid"){
        deusesGHM.push(god.name)
    }
}
console.log(deusesGHM)